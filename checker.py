#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import inspect
import traceback
import requests
import random
import string
from enum import Enum

""" <config> """
# SERVICE INFO
PORT = 8083

# DEBUG -- logs to stderr, TRACE -- verbose log
DEBUG = os.getenv("DEBUG", True)
TRACE = os.getenv("TRACE", False)
""" </config> """


def check(host):
    _log(f"Running CHECK on {host}")
    try:
        resp = requests.get(host)
    except requests.exceptions.RequestException as e:
        die(ExitStatus.DOWN,"connection failed")
    if resp.status_code==200:
        username="".join(random.SystemRandom().choice(string.ascii_letters+string.digits) for _ in range(12))
        url=host+'signup'
        resp = requests.post(url,{"username":username,"password":username})
        if resp.status_code==200:
            _log("register succeed")
            url=host+'auth'
            resp = requests.post(url,{"username":username,"password":username})
            if resp.status_code == 200:
                die(ExitStatus.OK,"login success")
            else:
                die(ExitStatus.MUMBLE,"login failed")
        else:
            die(ExitStatus.MUMBLE,"register failed")
        
    else:
        die(ExitStatus.DOWN,"not OK code")


def put(host, flag_id, flag, vuln):
    _log(f"Running PUT on {host} with {flag_id}:{flag}")
    url=host+'addRecipe'
    resp = requests.post(url,{"recipe":flag})
    if resp.status_code == 200:
        die(ExitStatus.OK, "Flag added successfully")
    else:
        _log(resp.text)
        die(ExitStatus.MUMBLE, "Problem with adding flag")


def get(host, flag_id, flag, vuln):
    _log(f"Running GET on {host} with {flag_id}:{flag}")


    die(ExitStatus.OK, "Not implemented")


""" <common> """


class ExitStatus(Enum):
    OK = 101
    CORRUPT = 102
    MUMBLE = 103
    DOWN = 104
    CHECKER_ERROR = 110


def _log(obj):
    if DEBUG and obj:
        caller = inspect.stack()[1].function
        print(f"[{caller}] {obj}", file=sys.stderr, flush=True)
    return obj


def die(code: ExitStatus, msg: str):
    if msg:
        print(msg, file=sys.stderr, flush=True)
    exit(code.value)


def _main():
    action, host, *args = sys.argv[1:]

    if not host.startswith("http"):
        host = f"http://{host}:{PORT}/"

    try:
        if action == "check":
            check(host)
        elif action == "put":
            flag_id, flag, vuln = args
            put(host, flag_id, flag, vuln)
        elif action == "get":
            flag_id, flag, vuln = args
            get(host, flag_id, flag, vuln)
        else:
            raise IndexError
    except IndexError:
        die(
            ExitStatus.CHECKER_ERROR,
            f"Usage: {sys.argv[0]} check|put|get IP FLAGID FLAG",
        )
    except Exception as e:
        die(
            ExitStatus.CHECKER_ERROR,
            f"Exception: {e}. Stack:\n {traceback.format_exc()}",
        )


""" </common> """

if __name__ == "__main__":
    _main()
